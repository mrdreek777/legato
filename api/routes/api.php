<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

Route::get('page','PageController@index')->name('page');
Route::get('add-job','PageController@addJobToQueue')->name('add-job');
Route::get('get-file-list','PageController@getFileList')->name('get-file-list');
Route::get('setPaymentState','PageController@setPaymentState')->name('payment');
Route::get('setPaid','PageController@setIsPayment')->name('paid]');

Route::post('file','FileController@post')->name('file.post');

Route::post('calculate','CalculatorController@calculate')->name('calculate');
