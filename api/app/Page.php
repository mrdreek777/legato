<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 04.02.19
 * Time: 16:10
 */

namespace App;

use Illuminate\Support\Carbon;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Page extends Eloquent
{
    protected $fillable = ['user_id', 'request_id', 'type'];

    protected $attributes = [
        'payment_state' => false,
        'file_loaded' => false,
        'paid' => false,
        'sub_folders' => [],
    ];

    public function checkExpired()
    {
        return Carbon::now()->diffInHours($this->updated_at, false) > 8;
    }

    public function getInfo()
    {
        return [
            'expired' => $this->checkExpired(),
            'isPaymentState' => $this->payment_state ?? false,
            'page_id' => $this->_id,
            'paid' => $this->paid,
            'type' => $this->type,
        ];
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }
}
