<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class File extends Eloquent
{
    protected $fillable = [
        'name',
        'path',
        'page_id',
        'type',
    ];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
