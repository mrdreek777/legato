<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 04.02.19
 * Time: 16:10
 */

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

// В будущем класс админки
class FileType extends Eloquent
{
    public const FILE_TYPES = [
        'gos_tickets' => 'квитанция об оплате госпошлины',
        'percent_files' => 'Расчет процентов по ст. 395 ГК РФ (его можно скать на ресурсе расчета)',
        'loan_files' => 'Копия Договора займа',
        'check_files' => 'Копия документа, подтверждающего передачу денежных средств',
        'debt_files' => 'Документы, подтверждающие частичное погашение задолженности (если такое имело место)',
        'passport_files' => 'Копия паспорта',
    ];

}
