<?php

namespace App\Jobs;

use App\FileType;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use MrDreek\b24rest\B24rest;

class TransferFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $deal;
    private $b24rest;
    private $mainFolderId;

    private $filesField;

    /**
     * Create a new job instance.
     *
     * @param $deal
     *
     * @throws \Exception
     */
    public function __construct($deal)
    {
        $this->deal = $deal;
        $this->b24rest = new B24rest;

        $this->mainFolderId = config('b24rest.folder_id');
        if ($this->mainFolderId === null) {
            throw new \Exception('InvalidConfigException');
        }

        $this->filesField = config('b24rest.files_field');
        if ($this->filesField === null) {
            throw new \Exception('InvalidConfigException');
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        Log::notice('Begin load file');
        if ($this->deal) {
            $deal = $this->deal;
            $downloadedFileUrls = [];
            $bDeal = $this->b24rest->getDeal($deal->request_id);
            if (isset($bDeal->result)) {
                $mainFolderName = preg_replace('/[^A-Za-z0-9А-яа-я\-\.\_\ ]/mu', '_', $bDeal->result->TITLE . '-' . Carbon::parse($bDeal->result->DATE_CREATE)->format('d.m.Y H:i'));

                if (!isset($deal->main_folder_id)) {
                    $mainFolderData = $this->b24rest->createSubFolder((int)$this->mainFolderId, $mainFolderName);
                    if (isset($mainFolderData->result)) {
                        $deal->main_folder_id = $mainFolderData->result->ID;
                        $deal->save();
                    } else {
                        Log::error('error. create main folder.' . json_encode($mainFolderData, JSON_UNESCAPED_UNICODE));
                        throw new \Exception();
                    }
                }

                foreach ($deal->files as $file) {
                    if (!$file->bitrixData) {
                        if (!isset($deal->sub_folders) || !array_key_exists($file->type, $deal->sub_folders)) {
                            $subFolderData = $this->b24rest->createSubFolder($deal->main_folder_id, FileType::FILE_TYPES[$file->type]);
                            if ($subFolderData->result) {
                                $subFolders = $deal->sub_folders;
                                $subFolders[$file->type] = $subFolderData->result->ID;
                                $deal->sub_folders = $subFolders;
                                $deal->save();
                            } else {
                                Log::error('error. create subfolder ' . json_encode($subFolderData, JSON_UNESCAPED_UNICODE));
                                throw new \Exception();
                            }
                        }

                        $fileData = $this->b24rest->uploadFile($deal->sub_folders[$file->type], $file->name, $file->path);
                        if (isset($fileData->result)) {
                            $downloadedFileUrls[] = $fileData->result->DETAIL_URL;
                            $file->bitrixData = $fileData->result;
                            $file->save();

                            unlink($file->path);
                        } else {
                            Log::error('error. file upload ' . json_encode($fileData, JSON_UNESCAPED_UNICODE));
                            throw new \Exception('error. file upload');
                        }

                    } else {
                        Log::notice('file loaded. Sleep 0.5 sec');
                        usleep(500);
                        Log::notice('resume work');
                    }
                }

                if (!empty($downloadedFileUrls)) {
                    $updateDealData = $this->b24rest->updateDeal($deal->request_id,
                        [
                            $this->filesField => $downloadedFileUrls,
                        ],
                        null);

                    if ($updateDealData->result) {
                        $deal->fileLinks = $downloadedFileUrls;
                        $deal->file_loaded = true;
                        $deal->save();
                        Log::notice('update dael');
                    } else {
                        Log::error('error. deal not be saved.' . json_encode($downloadedFileUrls, JSON_UNESCAPED_UNICODE));
                        throw new \Exception();
                    }
                }
                Log::notice('end import deal');
            } else {
                Log::error('error. get deal info. ' . json_encode($bDeal, JSON_UNESCAPED_UNICODE . "\n" . json_decode($deal, JSON_UNESCAPED_UNICODE)));
                throw new \Exception();
            }
        } else {
            Log::notice('new deal not found');
            throw new \Exception();
        }
    }
}
