<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Page;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     *
     * @param FileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function post(FileRequest $request)
    {
        $path = Storage::putFile('files', $request->file);
        if ($path) {
            $page = Page::where('_id', $request->page_id)->firstOrFail();
            $page->files()->create([
                'name' => $request->file->getClientOriginalName(),
                'path' => Storage::path($path),
                'type' => $request->type,
            ]);
            return Response::json('success', 200);
        }

        return Response::json('error', 400);
    }
}
