<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CalculatorController extends Controller
{
    public function calculate(Request $request): array
    {
        return ['sum' => (float)$request->input('a') + (float)$request->input('b') + (float)$request->input('c')];
    }
}
