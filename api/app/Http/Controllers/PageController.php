<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileListRequest;
use App\Http\Requests\PageIdRequest;
use App\Http\Requests\PageRequest;
use App\Jobs\TransferFileJob;
use App\Page;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use MrDreek\b24rest\B24rest;

class PageController extends Controller
{
    public function index(PageRequest $request)
    {
        $page = Page::firstOrCreate([
            'user_id' => $request->input('user_id'),
            'request_id' => $request->input('request_id'),
            'type' => $request->input('type'),
        ]);

        return $page->getInfo();
    }

    /**
     * @param FileListRequest $request
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Exception
     */
    public function getFileList(FileListRequest $request)
    {
        $type = $request->input('type');

        $types = json_decode(Storage::disk('config')->get('config.json'), JSON_UNESCAPED_UNICODE);

        if($type !== null){
            return $types[$type];
        }

        throw new \Exception('Invalid config');
    }

    public function setPaymentState(PageIdRequest $request)
    {
        $page = Page::where('_id', $request->page_id)->firstOrFail();
        $page->payment_state = true;
        $page->save();
        return $page->getInfo();
    }

    public function addJobToQueue(PageIdRequest $request)
    {
        $page = Page::where('_id', $request->page_id)->firstOrFail();
        try {
            dispatch(new TransferFileJob($page));
        } catch (\Exception $e) {
            Log::error('error new Transfer job. ' . json_encode($e, JSON_UNESCAPED_UNICODE));
        }
    }

    public function setIsPayment(PageIdRequest $request)
    {
        $page = Page::where('_id', $request->page_id)->firstOrFail();
        $response = (new B24rest)->updateDeal($page->request_id,
            [
                'STAGE_ID' => 'PREPARATION',
            ],
            null
        );

        if (isset($response->result)) {
            $page->paid = true;
            $page->save();
            return ['success' => true];
        }

        Log::error('error. set paid' . json_encode($response, JSON_UNESCAPED_UNICODE));
        return ['success' => false];
    }
}
