<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'request_id' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Требуется указать ID_USER',
            'request_id.required' => 'Требуется указать ID_REQUEST',
            'type.required' => 'Требуется указать TYPE',
        ];
    }
}
