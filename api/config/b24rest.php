<?php
/**
 * Created by PhpStorm.
 * User: mr_dreek
 * Date: 15.02.19
 * Time: 14:20
 */

return [
    'folder_id' => env('FOLDER_ID'),
    'url' => env('BITRIX_URL'),
    'files_field' => env('FILES_FIELD'),
];
