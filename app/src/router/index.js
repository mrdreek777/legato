import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export const constantRouterMap = [
  {
    path: "/",
    component: () => import("@/views/Page"),
    props: route => ({
      user_id: route.query.ID_USER,
      request_id: route.query.ID_REQUEST,
      type: route.query.TYPE
    })
  },
  {
    path: "/404",
    component: () => import("@/views/errorPage/404"),
    hidden: true
  },
  {
    path: "/401",
    component: () => import("@/views/errorPage/401"),
    hidden: true
  },
  {
    path: "/500",
    component: () => import("@/views/errorPage/500"),
    hidden: true
  },
  {
    path: "*",
    redirect: "/404",
    hidden: true
  }
];

export default new Router({
  mode: "history",
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
});
