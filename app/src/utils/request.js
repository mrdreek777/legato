import axios from 'axios'

const service = axios.create({
  headers: {
    post: {
      Accept: "application/json"
    },
    common: {
      Accept: "application/json"
    }
  },
  baseURL: process.env.BASE_API,
  timeout: 10000
});

service.interceptors.request.use(
  config => config,
  error => {
    console.log(error);
    Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => response,
  error => {
    console.log("err" + error);
    return Promise.reject(error);
  }
);

export default service;
