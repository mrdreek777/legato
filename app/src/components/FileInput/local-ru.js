export default {
  dictDefaultMessage: "Перенесите файлы сюда, чтобы загрузить их",
  dictFallbackMessage:
    "Ваш браузер не поддерживает загрузку файлов с помощью перетаскивания",
  dictFallbackText: "Используйте форму для загрузки ваших файлов",
  dictFileTooBig:
    "Файл слишком большой. Максимальный размер файла {{maxFileSize}} МБ",
  dictInvalidFileType: "Файл этого типа не может быть загружен",
  dictResponseError: "Сервер отклонил ваш запрос со статусом {{statusCode}}",
  dictCancelUpload: "Отменить загрузку",
  dictCancelUploadConfirmation: null,
  dictRemoveFile: "Удалить файл",
  dictMaxFilesExceeded: "Вы не можете загружать больше файлов"
};
