import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.use(VueSweetalert2);

Vue.config.productionTip = false;

let vue = new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
}).$mount("#app");

window.Vue = vue;

export default vue;
