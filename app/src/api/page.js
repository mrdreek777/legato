import request from '../utils/request'

export function getPage(request_id, user_id, type) {
  return request({
    url: "/api/page",
    method: "get",
    params: { request_id, user_id, type }
  });
}

export function setPaymentType(page_id) {
  return request({
    url: "/api/setPaymentState",
    method: "get",
    params: { page_id }
  });
}

export function setPaid(page_id) {
  return request({
    url: "/api/setPaid",
    method: "get",
    params: { page_id }
  });
}

export function addJobToQueue(page_id) {
  return request({
    url: "/api/add-job",
    method: "get",
    params: { page_id }
  });
}

export function getFileList(type) {
  return request({
    url: "/api/get-file-list",
    method: "get",
    params: { type }
  });
}
