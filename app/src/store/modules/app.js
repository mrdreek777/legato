import Vue from 'vue'
import _ from 'lodash'
import { getFileList } from '@/api/page'
import { arrayToObject } from '@/utils/arrayToObject'

export const state = {
  files: {},
  page_id: null,
  is_error: false
};

export const mutations = {
  UPDATE_STATE(state, { type, value }) {
    if (state.files[type]["value"] !== undefined) {
      Vue.set(state.files[type], "value", state.files[type]["value"] + value);
    } else {
      Vue.set(state.files[type], "value", value);
    }
  },
  SET_PAGE_ID(state, id) {
    state.page_id = id;
  },
  SET_IS_ERROR(state, flag) {
    state.is_error = flag;
  },
  SET_UPLOAD_STATE(state, { type, bool }) {
    Vue.set(state.files[type], "success", bool);
  },
  SET_FILE_LIST(state, files) {
    let myFiles = _.map(files, item => {
      return Object.assign(item, { success: false });
    });
    state.files = arrayToObject(myFiles, "type");
  }
};

export const getters = {
  files: state => state.files,
  page_id: state => state.page_id,
  is_error: state => state.is_error,
  valid: state => {
    let arr = _.map(state.files, item => {
      if (item.required) {
        if (item.value !== undefined) {
          return item.value !== 0;
        }

        return false;
      }
      return true;
    });

    if (arr.length) {
      return _.every(arr, Boolean);
    }

    return false;
  },
  uploadSuccess: state => {
    let arr = _.map(state.files, item => {
      if (item.required) {
        return item.success;
      } else {
        return true;
      }
    });

    if (arr.length) {
      return _.every(arr, Boolean);
    }

    return false;
  }
};

export const actions = {
  updateState({ commit }, { type, value }) {
    commit("UPDATE_STATE", { type, value });
  },
  setPageId({ commit }, id) {
    commit("SET_PAGE_ID", id);
  },
  setIsError({ commit }, bool) {
    commit("SET_IS_ERROR", bool);
  },
  setUploadState({ commit }, { type, bool }) {
    commit("SET_UPLOAD_STATE", { type, bool });
  },
  setFileList({ commit }, type) {
    return new Promise((resolve, reject) => {
      getFileList(type)
        .then(({ data }) => {
          commit("SET_FILE_LIST", data);
          resolve();
        })
        .catch(() => {
          reject();
        });
    });
  }
};
