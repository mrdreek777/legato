module.exports = {
  root: true,

  env: {
    node: true
  },

  extends: ["standard", "plugin:vue/essential", "@vue/prettier"],

  rules: {
    "no-console": "off",
    "no-debugger": "off"
  },

  parserOptions: {
    parser: "babel-eslint"
  },

  extends: ["standard", "plugin:vue/recommended", "@vue/prettier"]
};
